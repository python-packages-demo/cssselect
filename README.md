# [cssselect](https://pypi.org/project/cssselect/)

Parses CSS3 Selectors and translates them to XPath 1.0 https://cssselect.readthedocs.io/

Unofficial howto and demo

## In other languages than Python
### PHP
* [symfony](https://phppackages.org/s/symfony)/[css-selector](https://phppackages.org/p/symfony/css-selector)